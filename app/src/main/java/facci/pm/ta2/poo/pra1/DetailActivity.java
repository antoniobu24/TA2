package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");

        //PUNTO 3.1
        //EN ESTE CODIGO SE RECCIVE LOS DATOS DEL OBJECT ID
        String object_id = getIntent().getStringExtra("object_id");

        //SE DEFINE EL CAMPO DESCRIPCION PARA PODER ESTABLECER LA FUNCION DE SCROLL CUANDO EL TEXTO SEA MAYOR A 150DIP
        TextView description = (TextView) findViewById(R.id.textViewDescription);
        description.setMovementMethod(LinkMovementMethod.getInstance());

        // INICIO - CODE6

        //SE ACCEDE AL OBJECT RECIVIDO COMO PARÁMETTRO EN LA ACTIVIDAD
        DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id, new GetCallback<DataObject>() {

            @Override
            public void done(DataObject object, DataException e) {

                if (e ==null){ //SE PREGUNTA SI EL OBJETO ES DISTINTO A NULL SE OBTIENEN LOS SIGUIENTES DATOS

                    //PUNTO 3.3

                    //A CONTINUACION SE DECLARAN LAS PROPIEDADES O CAMPOS DEL OBJETO
                    TextView title = (TextView) findViewById(R.id.textViewName);
                    TextView price = (TextView) findViewById(R.id.textViewPrice);
                    TextView description = (TextView) findViewById(R.id.textViewDescription);
                    ImageView thumbnail = (ImageView) findViewById(R.id.thumbnail);

                    //PUNTO 3.4

                    //EN LAS SIGUIENTES LINEAS DE CODIGO SE OBTIENE LA INFORMACIÓN QUE RELLENA LOS CAMPOS QUE ANTES DEFINIMOS
                    //ADEMÁS SE ESTABLECE EL CODIGO UNICODE DEL SIGNO DE $
                    title.setText((String) object.get("name"));
                    price.setText((String) object.get("price") + "\u0024");
                    description.setText((String) object.get("description"));
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));

                }else{
                    //Error
                }
            }
        });

        // FIN - CODE6

    }

}
